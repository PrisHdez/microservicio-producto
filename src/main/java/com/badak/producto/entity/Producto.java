package com.badak.producto.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="productos")
public class Producto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idProducto;
	
	private String clave;
	
	private String producto;
	
	private BigDecimal precio;
	
	private int banDisponibilidad;

	public Long getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	public void setPrecio(BigDecimal precio) {
		this.precio = precio;
	}

	public int getBanDisponibilidad() {
		return banDisponibilidad;
	}

	public void setBanDisponibilidad(int banDisponibilidad) {
		this.banDisponibilidad = banDisponibilidad;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Producto [idProducto=");
		builder.append(idProducto);
		builder.append(", clave=");
		builder.append(clave);
		builder.append(", producto=");
		builder.append(producto);
		builder.append(", precio=");
		builder.append(precio);
		builder.append(", banDisponibilidad=");
		builder.append(banDisponibilidad);
		builder.append("]");
		return builder.toString();
	}
	
	
}
