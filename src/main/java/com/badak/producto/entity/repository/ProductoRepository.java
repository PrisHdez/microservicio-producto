package com.badak.producto.entity.repository;

import org.springframework.data.repository.CrudRepository;

import com.badak.producto.entity.Producto;

public interface ProductoRepository extends CrudRepository<Producto, Long> {

}
