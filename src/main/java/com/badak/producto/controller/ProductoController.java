package com.badak.producto.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.badak.producto.entity.Producto;
import com.badak.producto.entity.service.ProductoService;

@RestController
@RequestMapping("/Product") 
public class ProductoController {

	@Autowired
	private ProductoService productoService;
	
	@RequestMapping(path = "/products/" ,method = RequestMethod.GET)
	public ResponseEntity<?> consultarProductos() {
		return ResponseEntity.ok().body(productoService.findAll());
	}
	
	@RequestMapping(path = "/products/{id}" ,method = RequestMethod.GET)
	public ResponseEntity<?> consultarProductoById(@PathVariable  Long id) {
		Optional<Producto> productoBd = productoService.findById(id);
		
		if (!productoBd.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(productoBd.get());
	}
	
	
	@RequestMapping(path = "/products/" ,method = RequestMethod.PUT)
	public ResponseEntity<?> guardarProducto(@RequestBody Producto producto) {
		Producto productobd = productoService.save(producto);
		return ResponseEntity.status(HttpStatus.CREATED).body(productobd);
	}
	
	@RequestMapping(path = "/products/{id}" ,method = RequestMethod.DELETE)
	public  ResponseEntity<?> eliminarProducto(@PathVariable  Long id) {
		productoService.deleteById(id);
		return ResponseEntity.noContent().build();
	}

	@RequestMapping(path = "/products/{id}" ,method = RequestMethod.PUT)
	public ResponseEntity<?> actualizarProducto(@RequestBody Producto producto,@PathVariable  Long id) {
		Optional<Producto> productoBd = productoService.findById(id);
		
		if (!productoBd.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		Producto productoPersist = productoBd.get();
		
		productoPersist.setClave(producto.getClave());
		productoPersist.setBanDisponibilidad(producto.getBanDisponibilidad());
		productoPersist.setPrecio(producto.getPrecio());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(productoService.save(productoPersist));
	}
}
